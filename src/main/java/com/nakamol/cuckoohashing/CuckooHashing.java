/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.nakamol.cuckoohashing;

/**
 *
 * @author OS
 */
public class CuckooHashing {

    private int key;
    private String value;
    private CuckooHashing[] FindLocate = new CuckooHashing[50];
    private CuckooHashing[] FindLocate2 = new CuckooHashing[50];

    public CuckooHashing(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public CuckooHashing() {

    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public int hashMap(int key) {
        return key % FindLocate.length;
    }

    public int hashMap2(int key) {
        return (key / 97) % FindLocate2.length;
    }

    public void input(int key, String value) {

        if (FindLocate[hashMap(key)] != null && FindLocate[hashMap(key)].key == key) {
            FindLocate[hashMap(key)].key = key;
            FindLocate[hashMap(key)].value = value;
            return;
        }
        if (FindLocate2[hashMap2(key)] != null && FindLocate2[hashMap2(key)].key == key) {
            FindLocate2[hashMap2(key)].key = key;
            FindLocate2[hashMap2(key)].value = value;
            return;
        }
        int i = 0;

        // replace
        while (true) {
            if (i == 0) {
                if (FindLocate[hashMap(key)] == null) {
                    FindLocate[hashMap(key)] = new CuckooHashing();
                    FindLocate[hashMap(key)].key = key;
                    FindLocate[hashMap(key)].value = value;
                    return;
                }
            } else {
                if (FindLocate2[hashMap2(key)] == null) {
                    FindLocate2[hashMap2(key)] = new CuckooHashing();
                    FindLocate2[hashMap2(key)].key = key;
                    FindLocate2[hashMap2(key)].value = value;
                    return;
                }
            }

            // excise
            if (i == 0) {
                String tv = FindLocate[hashMap(key)].value;
                int tk = FindLocate[hashMap(key)].key;
                FindLocate[hashMap(key)].key = key;
                FindLocate[hashMap(key)].value = value;
                key = tk;
                value = tv;

            } else {
                String tv2 = FindLocate2[hashMap2(key)].value;
                int tk2 = FindLocate2[hashMap2(key)].key;
                FindLocate2[hashMap2(key)].key = key;
                FindLocate2[hashMap2(key)].value = value;
                key = tk2;
                value = tv2;
            }
            i = (i + 1) % 2;
        }
    }

    public CuckooHashing get(int key) {
        if (FindLocate[hashMap(key)] != null && FindLocate[hashMap(key)].key == key) {
            return FindLocate[hashMap(key)];
        }
        if (FindLocate2[hashMap2(key)] != null && FindLocate2[hashMap2(key)].key == key) {
            return FindLocate2[hashMap2(key)];
        }
        return null;
    }

    public CuckooHashing delete(int key) {
        if (FindLocate[hashMap(key)] != null && FindLocate[hashMap(key)].key == key) {
            CuckooHashing temp = FindLocate[hashMap(key)];
            FindLocate[hashMap(key)] = null;
            return temp;
        }
        if (FindLocate2[hashMap2(key)] != null && FindLocate2[hashMap2(key)].key == key) {
            CuckooHashing temp = FindLocate2[hashMap2(key)];
            FindLocate2[hashMap2(key)] = null;
            return temp;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Key: " + key + "  Value=" + value;
    }

}
