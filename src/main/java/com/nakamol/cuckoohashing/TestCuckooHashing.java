/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nakamol.cuckoohashing;

/**
 *
 * @author OS
 */
public class TestCuckooHashing {

    public static void main(String[] args) {
        CuckooHashing cuckHashing = new CuckooHashing();
        cuckHashing.input(102, "Nakamol");
        cuckHashing.input(97, "Khim");
        cuckHashing.input(127, "Kenji");
        cuckHashing.input(150, "Best");
        System.out.println(cuckHashing.get(102).toString());
        System.out.println(cuckHashing.get(97).toString());
        System.out.println(cuckHashing.get(127).toString());
        System.out.println(cuckHashing.get(150).toString());
        cuckHashing.delete(97);
        System.out.println(cuckHashing.get(97));
    }
}
